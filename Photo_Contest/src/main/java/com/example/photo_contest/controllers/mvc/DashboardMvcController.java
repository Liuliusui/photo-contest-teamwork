package com.example.photo_contest.controllers.mvc;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.AuthenticationFailureException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.models.*;
import com.example.photo_contest.services.contracts.ContestService;
import com.example.photo_contest.services.contracts.SubmissionService;
import com.example.photo_contest.services.contracts.UserPointCalculation;
import com.example.photo_contest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/dashboard")
public class DashboardMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final SubmissionService submissionService;
    private final UserPointCalculation userPointCalculation;

    @Autowired
    public DashboardMvcController(UserService userService, AuthenticationHelper authenticationHelper, ContestService contestService, SubmissionService submissionService, UserPointCalculation userPointCalculation) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.submissionService = submissionService;
        this.userPointCalculation = userPointCalculation;
    }
    @ModelAttribute
    private void pointCalculation(){
        userPointCalculation.pointCalculation();
    }

    @ModelAttribute("isAuthenticated")
    public boolean popularIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (popularIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getId();
        return userId;
    }

    @ModelAttribute("currentlyLoggedInUser")
    public User getCurrentlyLoggedInUser(HttpSession session) {
        User user = null;
        if (popularIsAuthenticated(session)) user = authenticationHelper.tryGetUser(session);
        return user;
    }

    @GetMapping
    public String showUserDashboard(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getActiveOpen();
            String UsersCount = String.valueOf(userService.getUsersCount());
            model.addAttribute("usersCount", UsersCount);
            model.addAttribute("contests", contests);
            boolean isOrganizer = isOrganizer(user);
            if (isOrganizer) {
                return "redirect:/dashboard/all";
            } else {
                return "dashboard";
            }
        } catch (AuthenticationFailureException e) {
            return "not-authorized";
        }

    }

    private boolean isOrganizer(User user) {
        Set<Role> roles = user.getRoles();
        boolean isOrg = false;
        for (Role role : roles) {
            if (role.getName().equals("Organizer")) {
                isOrg = true;
            }
        }
        return isOrg;
    }

    @GetMapping("/finished")
    public String showUserDashboardFinishedOpen(Model model) {
        List<Contest> contests = contestService.getFinishedOpen();
        model.addAttribute("contests", contests);
        return "dashboard-finished-open";

    }

    @GetMapping("/current")
    public String showUserDashboardCurrentParticipating(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        int userId = user.getId();
        List<Contest> contests = contestService.getCurrent(userId);
        model.addAttribute("contests", contests);
        return "dashboard-current";
    }


    @GetMapping("/past")
    public String showUserDashboardPastParticipating(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        int userId = user.getId();
        List<Contest> contests = contestService.getPast(userId);
        model.addAttribute("contests", contests);
        return "dashboard-past";
    }


    @GetMapping("/all")
    public String showAllContests(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getAll(user);
            model.addAttribute("contests", contests);
            return "organizer-dashboard-all";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/phaseone")
    public String showPhaseOneContests(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getPhaseOne(user);
            model.addAttribute("contests", contests);
            return "organizer-dashboard-phase-one";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/phasetwo")
    public String showPhaseTwoContests(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getPhaseTwo(user);
            model.addAttribute("contests", contests);
            return "organizer-dashboard-phase-two";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/all-finished")
    public String showPhaseAllFinished(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getFinished(user);
            model.addAttribute("contests", contests);
            return "organizer-dashboard-all-finished";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/invitations")
    public String showInvitations(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getInvited(user);
            model.addAttribute("contests", contests);
            return "dashboard-invited";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("{id}")
    public String showPhaseTwoSingle(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(id);
            Set<Photo> photos = contest.getPhotos();
            model.addAttribute("contest", contest);
            model.addAttribute("photos", photos);
            model.addAttribute("user", user);
            return "contest-phase-two";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }
}
