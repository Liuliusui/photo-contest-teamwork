package com.example.photo_contest.controllers.rest;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.User;
import com.example.photo_contest.services.contracts.ContestService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;

    public ContestController(ContestService contestService, AuthenticationHelper authenticationHelper) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Contest> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Contest getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.getById(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Contest> filter(@RequestHeader HttpHeaders headers,
                                @RequestParam(required = false) Optional<String> title,
                                @RequestParam(required = false) Optional<Integer> categoryId,
                                @RequestParam(required = false) Optional<String> phase,
                                @RequestParam(required = false) Optional<String> sort) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.filter(user, title, categoryId, phase, sort);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}