package com.example.photo_contest.controllers.mvc;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.AuthenticationFailureException;
import com.example.photo_contest.exceptions.DuplicateEmailException;
import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.DuplicateUserException;
import com.example.photo_contest.models.dto.LoginDto;
import com.example.photo_contest.models.dto.RegisterDto;
import com.example.photo_contest.models.User;
import com.example.photo_contest.utils_mappers.ModelMapper;
import com.example.photo_contest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;


    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper, ModelMapper modelMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            return "redirect:/dashboard";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "The password confirmation should match the password.");
            return  "register";
        }

        try {
              User user = modelMapper.fromDto(register);
              userService.create(user);
            return "redirect:/auth/login";
        } catch (DuplicateUserException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }catch (DuplicateEmailException e ){
            bindingResult.rejectValue("email", "email.error", e.getMessage());
            return "register";
        }
    }

}
