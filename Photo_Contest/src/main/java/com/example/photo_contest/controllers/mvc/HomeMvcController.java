package com.example.photo_contest.controllers.mvc;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.models.Contest;
import com.example.photo_contest.services.contracts.ContestService;
import com.example.photo_contest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final ContestService contestService;
    public final AuthenticationHelper authenticationHelper;


    public HomeMvcController(UserService userService, ContestService contestService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean popularIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (popularIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getId();
        return userId;
    }


    @GetMapping
    public String showHomePage(Model model) {
        String UsersCount = String.valueOf(userService.getUsersCount());
        List<Contest> contests = contestService.getLatest();
        model.addAttribute("usersCount", UsersCount);
        model.addAttribute("contests", contests );
        return "index";
    }

}
