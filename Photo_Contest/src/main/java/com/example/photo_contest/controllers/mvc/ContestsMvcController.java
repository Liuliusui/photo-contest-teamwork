package com.example.photo_contest.controllers.mvc;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.AuthenticationFailureException;
import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.helpers.ImageUploadHelper;
import com.example.photo_contest.models.*;
import com.example.photo_contest.models.dto.ContestDto;
import com.example.photo_contest.models.dto.PhotoDto;
import com.example.photo_contest.services.contracts.*;
import com.example.photo_contest.utils_mappers.ModelMapper;
import com.example.photo_contest.utils_mappers.SubmissionMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/contests")
public class ContestsMvcController {

    private final UserService userService;
    private final ContestService contestService;
    private final CategoryService categoryService;
    private final PhotoService photoService;
    private final SubmissionService submissionService;
    public final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final SubmissionMapper submissionMapper;
    private final UserPointCalculation userPointCalculation;

    public ContestsMvcController(UserService userService, ContestService contestService, CategoryService categoryService, PhotoService photoService, SubmissionService submissionService, AuthenticationHelper authenticationHelper, ModelMapper modelMapper, SubmissionMapper submissionMapper, UserPointCalculation userPointCalculation) {
        this.userService = userService;
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.photoService = photoService;
        this.submissionService = submissionService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.submissionMapper = submissionMapper;
        this.userPointCalculation = userPointCalculation;
    }
    @ModelAttribute
    private void pointCalculation(){
        userPointCalculation.pointCalculation();
    }

    @ModelAttribute("isAuthenticated")
    public boolean popularIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean isOrganizer(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        Set<Role> roles = user.getRoles();
        boolean isOrganizer = false;
        for (Role role : roles) {
            if (role.getName().equals("Organizer")) {
                isOrganizer = true;
            }
        }
        return isOrganizer;
    }


    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (popularIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getId();
        return userId;
    }

    @ModelAttribute("currentlyLoggedInUser")
    public User getCurrentlyLoggedInUser(HttpSession session) {
        User user = null;
        if (popularIsAuthenticated(session)) user = authenticationHelper.tryGetUser(session);
        return user;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("photoJunkies")
    public List<User> getPhotoJunkies() {
        return userService.getAll().stream().filter(user1 -> !user1.isOrganizer()).collect(Collectors.toList());
    }

    @ModelAttribute("photos")
    public List<Photo> populatePhotos() {
        return photoService.getAll();
    }

    @GetMapping("/create-contest")
    public String showNewContestPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            List<User> users = userService.getAll();
            model.addAttribute("users", users);
            model.addAttribute("contest", new ContestDto());
            return "create-contest";

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

    }

    @PostMapping("/create-contest")
    public String createContest(@Valid @ModelAttribute("contest") ContestDto contestDto, Model model, @RequestHeader HttpHeaders headers, @RequestParam(name="image", required=false) MultipartFile multipartFile, BindingResult errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "create-contest";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            Contest newContest = modelMapper.fromDto(contestDto);
            String link = ImageUploadHelper.uploadToCloudinary(multipartFile);
            if (!multipartFile.isEmpty()) {
                newContest.setCoverPhoto(link);
             }
            else {
                String altLink = contestDto.getCoverPhoto().replace (",", "").replace(", ", "").replace(" ", "");
                newContest.setCoverPhoto(altLink);
            }
            int[] participantInvitation = contestDto.getUsers();
            int[] juryInvitation = contestDto.getJury();
            contestService.create(newContest, participantInvitation,juryInvitation);
            return "redirect:/dashboard";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            return "not-authorized";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "dashboard";

    }


    @GetMapping("/{id}")
    public String getSingleContest(@PathVariable int id, Model model, HttpSession session) {
        Date current = new Date();
        try {
            User user = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(id);
            Set<Photo> photos = contest.getPhotos();
            boolean isUserParticipate = isUserParticipate(contest, user);
            Set<Photo> singlePhoto = userPhoto(contest, user);
            PhotoDto photoDto = new PhotoDto();
            model.addAttribute("photos", photos);
            model.addAttribute("isUserParticipate", isUserParticipate);
            model.addAttribute("user", user);
            model.addAttribute("contest", contest);
            model.addAttribute("singlePhoto", singlePhoto);
            model.addAttribute("newPhoto", photoDto);
            if (contest.isPhaseOne()) {
                return "contest-phase-one";
            }else if (contest.isPhaseTwo() && user.isOrganizer()){
                return "contest-phase-one";
            }else if (contest.isPhaseTwo() && !user.isOrganizer()){
                return "contest-phase-one";
            }
            return "contest";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";
        }
    }

    private Set<Photo> userPhoto(Contest contest, User user) {

        Set<Photo> userPhoto =new HashSet<>();
        for (Photo photo: contest.getPhotos()) {
            if (photo.getUser().getId() == user.getId()){
                userPhoto.add(photo);
            }
        }
        return userPhoto;
    }


    private boolean isUserParticipate(Contest contest, User user) {

        boolean isParticipate = false;
        for (Photo photo : contest.getPhotos()) {
            if (photo.getUser().getId() == user.getId())
                isParticipate = true;
        }
        return isParticipate;
    }



    @PostMapping("/{id}/submission")
    public String makeASubmission(@PathVariable int id,
                                  @RequestHeader HttpHeaders headers,
                                  @Valid @ModelAttribute("newPhoto") PhotoDto photoDto,
                                  @RequestParam("image") MultipartFile multipartFile,
                                  BindingResult errors,
                                  Model model,
                                  HttpSession session) {
        if (errors.hasErrors()) {
            return "not-found";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(id);
            int jurySize = contest.getJury().size();
            String link = ImageUploadHelper.uploadToCloudinary(multipartFile);
            Photo newPhoto = modelMapper.dtoToObject(photoDto, user, contest, jurySize);
            newPhoto.setPhoto(link);
            newPhoto.setUser(user);
            newPhoto.setContest(contest);
            newPhoto.setBlocked(false);
            photoService.create(newPhoto);
            Contest contest1 = contestService.getById(newPhoto.getContest().getId());
            Set<User> jury = contest.getJury();
            for (User juryman: jury) {
                Submission submission = submissionMapper.DefaultDtoToObject(juryman, newPhoto);
                submissionService.create(submission);
            }
            model.addAttribute("contest", contest);
            model.addAttribute("image", multipartFile);
            return "redirect:/contests/{id}";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "dashboard";
    }

    @GetMapping("/invitations-phase-one")
    public String getJuryInvitationsPhaseOne(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getInvitationsPhaseOne(user);
            model.addAttribute("contests", contests);
            return "jury-invitations-phase-one";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";
        }
    }


    @GetMapping("/invitations-phase-two")
    public String getJuryInvitationsPhaseTwo(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> contests = contestService.getInvitationsPhaseTwo(user);
            model.addAttribute("contests", contests);
            return "jury-invitations-phase-two";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";
        }
    }



}
