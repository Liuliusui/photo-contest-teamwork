package com.example.photo_contest.exceptions;

public class DuplicateUserException extends RuntimeException {
    public DuplicateUserException(String username) {
        super(String.format("User with username %s already exists.", username));
    }
}
