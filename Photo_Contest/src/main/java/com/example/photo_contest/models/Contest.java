package com.example.photo_contest.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.thymeleaf.util.DateUtils;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "cover_photo")
    private String coverPhoto;

    @Column(name = "phase_1_start")
    private Date phaseOneStart;

    @Column(name = "phase_1_end")
    private Date phaseOneEnd;

    @Column(name = "phase_2_start")
    private Date phaseTwoStart;

    @Column(name = "phase_2_end")
    private Date phaseTwoEnd;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "is_open")
    private boolean isOpen;

    @Column(name = "is_calculated")
    private boolean isCalculated;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Set<Photo> photos;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "jury_contest",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jury;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_user_invitations",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> invitedParticipants;

    public Contest() {
    }

    public Contest(int id, String title, String description, String coverPhoto, Date phaseOneStart,
                   Date phaseOneEnd, Date phaseTwoStart, Date phaseTwoEnd, Category category, boolean isOpen) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.coverPhoto = coverPhoto;
        this.phaseOneStart = phaseOneStart;
        this.phaseOneEnd = phaseOneEnd;
        this.phaseTwoStart = phaseTwoStart;
        this.phaseTwoEnd = phaseTwoEnd;
        this.category = category;
        this.isOpen = isOpen;
    }

    public boolean isCalculated() {
        return isCalculated;
    }

    public void setCalculated(boolean calculated) {
        isCalculated = calculated;
    }

    public Set<User> getJury() {
        return jury;
    }

    public Set<User> getInvitedParticipants() {
        return invitedParticipants;
    }

    public void setInvitedParticipants(Set<User> invitedParticipants) {
        this.invitedParticipants = invitedParticipants;
    }

    public void setJury(Set<User> jury) {
        this.jury = jury;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public Date getPhaseOneStart() {
        return phaseOneStart;
    }

    public void setPhaseOneStart(Date phaseOneStart) {
        this.phaseOneStart = phaseOneStart;
    }

    public Date getPhaseOneEnd() {
        return phaseOneEnd;
    }

    public void setPhaseOneEnd(Date phaseOneEnd) {
        this.phaseOneEnd = phaseOneEnd;
    }

    public Date getPhaseTwoStart() {
        return phaseTwoStart;
    }

    public void setPhaseTwoStart(Date phaseTwoStart) {
        this.phaseTwoStart = phaseTwoStart;
    }

    public Date getPhaseTwoEnd() {
        return phaseTwoEnd;
    }

    public void setPhaseTwoEnd(Date phaseTwoEnd) {
        this.phaseTwoEnd = phaseTwoEnd;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    public boolean isPhaseOne () {
        Date now = new Date();
        return ((phaseOneStart.before(now) || phaseOneStart.equals(now)) && ((phaseOneEnd.after(now) || phaseOneEnd.equals(now))));
    }

    public boolean isPhaseTwo(){
        Date now = new Date();
        return  ((phaseTwoStart.before(now) || phaseTwoStart.equals(now)) && ((phaseTwoEnd.after(now) || phaseTwoEnd.equals(now))));
    }

    public boolean isFinished(){
        return phaseTwoEnd.before(new Date());
    }

}
