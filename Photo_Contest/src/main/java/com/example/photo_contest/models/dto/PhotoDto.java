package com.example.photo_contest.models.dto;

public class PhotoDto {


    private String title;


    private String story;

    private String photo;

    private int userid;

    private int contestId;

    private int score;

    public PhotoDto() {
    }

    public PhotoDto(String title, String story, String photo, int userid, int contestId) {
        this.title = title;
        this.story = story;
        this.photo = photo;
        this.userid = userid;
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }


}
