package com.example.photo_contest.models.dto;

public class ReviewDto {





    private int points;

    private String comment;

    public ReviewDto() {

    }

    public ReviewDto( int points, String comment) {

        this.points = points;
        this.comment = comment;
    }


    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
