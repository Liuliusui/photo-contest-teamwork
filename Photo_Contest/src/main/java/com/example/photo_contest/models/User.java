package com.example.photo_contest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @JsonIgnore
    @Column
    private String password;

    @Column(name = "story")
    private String story;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    @Column(name = "ranking")
    private int ranking;

    @Column(name = "date")
    private LocalDateTime date;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private Set<Photo> photos;

    @JsonIgnore
    @ManyToMany(mappedBy = "jury")
    private Set<Contest> juryInvitations;

    @JsonIgnore
    @ManyToMany(mappedBy = "invitedParticipants")
    private Set<Contest> contestInvitations;


    private String avatar;

    public User() {
    }

    public User(int id, String firstName, String lastName, String username, String email, String password, String avatar, String story) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.avatar = avatar;
        this.story = story;
        date = LocalDateTime.now();

    }

    public Set<Contest> getContestInvitations() {
        return contestInvitations;
    }

    public void setContestInvitations(Set<Contest> contestInvitations) {
        this.contestInvitations = contestInvitations;
    }

    public Set<Contest> getJuryInvitations() {
        return juryInvitations;
    }

    public void setJuryInvitations(Set<Contest> juryInvitations) {
        this.juryInvitations = juryInvitations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }


    public boolean isOrganizer() {
        return roles.stream().anyMatch(r -> r.getName().equals("Organizer"));
    }

    public void setOrganizer() {
        Role role = new Role();
        role.setId(1);
        role.setName("Organizer");
        roles.add(role);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    public int getFirstPrizes () {
        return (int) getPhotos().stream().filter(photo -> photo.getRanking() == 1).count();
    }

    public int getSecondPrizes () {
        return (int) getPhotos().stream().filter(photo -> photo.getRanking() == 2).count();
    }

    public int getThirdPrizes () {
        return (int) getPhotos().stream().filter(photo -> photo.getRanking() == 3).count();
    }

}
