package com.example.photo_contest.models;


import javax.persistence.*;

@Entity
@Table(name = "photo")
public class Photo  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @Column(name = "photo")
    private String photo;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @Column(name = "score")
    private int score;


     @Column(name = "ranking")
     private int ranking;


    @Column(name = "is_blocked")
    private boolean isBlocked;

    @Column(name = "is_invited")
    private boolean isInvited;

    public Photo() {
    }

    public Photo(int id, String title, String story, String photo, User user, Contest contest, int score) {
        this.id = id;
        this.photo = photo;
        this.title = title;
        this.story = story;
        this.user = user;
        this.contest = contest;
        this.score = score;

    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }

    public int getId() {
        return id;
    }

    public void setId(int photoId) {
        this.id = photoId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }


     public int getRanking() {
         return ranking;
     }

     public void setRanking(int ranking) {
         this.ranking = ranking;
     }

    public int getTotalScore() {
        return 1;
    }
}
