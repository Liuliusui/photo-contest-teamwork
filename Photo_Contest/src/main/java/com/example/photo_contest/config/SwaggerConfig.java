package com.example.photo_contest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Open on -> http://localhost:8080/swagger-ui/ */

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.example.photo_contest"))
                .paths(regex("/api.*"))
                .build()
                .apiInfo(apiDetails())
                .securityContexts(List.of(securityContext()))
                .securitySchemes(List.of(apiKey()));

    }
    private ApiInfo apiDetails() {
        return new ApiInfoBuilder()
                .title("Clean photography")
                .description(" A place where you can take part in photo contests and share your creativity.\n\n" +
                        "   1. Users\n" +
                        "      •\tCRUD operations (*must*)\n" +
                        "      •\tList and search by username, first name or last name (*must*)\n" +
                        "\n" +
                        "   2. Contests\n" +
                        "      •\tCRUD Operations (*must*)\n" +
                        "      •\tSubmit photo (*must*)\n" +
                        "      •\tRate photo (*must*)\n" +
                        "      •\tList and filter by title, category, type and phase (*must*)\n" +
                        "\n" +
                        "   3. Photos\n" +
                        "      •\tCRD operations (*must*)\n" +
                                "List and search by title (*must*)\n"
                        )

                .termsOfServiceUrl("https://gitlab.com/m.karalev/photo-contest-teamwork")
                .license("Developed by: Niya Tsaneva and Martin Karalev")
                .licenseUrl("https://gitlab.com/m.karalev/photo-contest-teamwork")
                .version("0.0.1")
                .build();
    }
    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return List.of(new SecurityReference("JWT", authorizationScopes));
    }
}
