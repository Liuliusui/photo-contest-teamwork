package com.example.photo_contest.services.contracts;

import com.example.photo_contest.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    void create(Category category);

    void update(Category category);

    void delete(int id);
}
