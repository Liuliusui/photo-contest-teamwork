package com.example.photo_contest.services.contracts;

import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.User;

import java.util.List;
import java.util.Optional;

public interface ContestService {

    List<Contest> getAll( User user);

    List<Contest> getPhaseOne(User user);

    List<Contest> getPhaseTwo(User user);

    List<Contest> getFinished(User user);

    List<Contest> getInvited(User user);

    Contest getById(int id);

    void create(Contest contest, int[] invitedParticipants, int[] invitedJury);

    void update(Contest contest);

    List<Contest> getLatest();

    List<Contest> getActiveOpen();

    List<Contest> getFinishedOpen();

    List<Contest> getCurrent(int userId);

    List<Contest> getPast(int userId);

    List<Contest> getInvitationsPhaseOne(User user);

    List<Contest> getInvitationsPhaseTwo(User user);

    List<Contest> getFinishedUnCalculated();

    List<Contest> filter(User user, Optional<String> title,Optional<Integer> categoryId, Optional<String> phase, Optional<String> sort);
}
