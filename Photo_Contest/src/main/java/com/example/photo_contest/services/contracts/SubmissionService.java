package com.example.photo_contest.services.contracts;

import com.example.photo_contest.models.Category;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;

import java.util.List;

public interface SubmissionService {

    void create(Submission submission);

    List<Submission> getAll(User user);

    Submission getById(int id);


    void update(Submission submission, User user);

    Submission getByUserIdPhotoId(int userId, int photoId);

    List<Submission>  getByPhotoId( int photoId);

}
