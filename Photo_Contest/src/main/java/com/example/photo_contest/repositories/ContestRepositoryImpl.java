package com.example.photo_contest.repositories;

import com.example.photo_contest.models.Contest;
import com.example.photo_contest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(Contest.class, sessionFactory);
    }


    @Override
    public Contest getByName(String title) {
        return getByField("title", title);
    }


    @Override
    public List<Contest> getFinishedOpen() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where isOpen = true and phaseTwoEnd < current_date ");
            return query.list();
        }
    }


    @Override
    public List<Contest> getActiveOpen() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where isOpen = true and phaseOneEnd >= current_date ");
            return query.list();
        }
    }

    @Override
    public List<Contest> getCurrent(int userId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Contest> query = session.createNativeQuery(
                    "select distinct contest.*, p.user_id " +
                            "from contest " +
                            "right join photo p on p.contest_id = contest.contest_id " +
                            "where user_id = :id " +
                            "and phase_2_start <= NOW() and phase_2_end >= NOW() ");
            query.setParameter("id", userId);
            query.addEntity(Contest.class);
            return query.list();
        }
    }

    @Override
    public List<Contest> getPast(int userId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Contest> query = session.createNativeQuery(
                    "select distinct contest.*, p.user_id " +
                            "from contest " +
                            "right join photo p on p.contest_id = contest.contest_id " +
                            "where user_id = :id " +
                            "and phase_2_end < NOW() ");
            query.setParameter("id", userId);
            query.addEntity(Contest.class);
            return query.list();
        }

    }

    @Override
    public List<Contest> getPhaseOne() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where current_date BETWEEN phaseOneStart and phaseOneEnd");
            return query.list();
        }

    }

    @Override
    public List<Contest> getPhaseTwo() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where current_date BETWEEN phaseTwoStart and phaseTwoEnd");
            return query.list();
        }
    }

    @Override
    public List<Contest> getFinished() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where phaseTwoEnd < current_date ");
            return query.list();
        }
    }

    @Override
    public List<Contest> getInvited(int userId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Contest> query = session.createNativeQuery(
                    "select *\n" +
                            "    from contest\n" +
                            "    join contest_user_invitations cui on contest.contest_id = cui.contest_id\n" +
                            "    where user_id = :id ");
            query.setParameter("id", userId);
            query.addEntity(Contest.class);
            return query.list();
        }
    }




    @Override
    public List<Contest> getFinishedUnCalculated() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where phaseTwoEnd < current_date and isCalculated = false ");
            return query.list();
        }
    }

    @Override
    public List<Contest> filter(Optional<String> title, Optional<Integer> categoryId, Optional<String> phase, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Contest");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            title.ifPresent(value -> {
                filter.add(" title like :title ");
                queryParams.put("title", "%" + value + "%");

            });

            categoryId.ifPresent(value -> {
                filter.add(" category.id = :category ");
                queryParams.put("category", value);
            });

            if (phase.isPresent()) {


                switch (phase.get()) {
                    case "one":
                        phase.ifPresent(value -> {
                            filter.add(" current_date BETWEEN phaseOneStart and phaseOneEnd");
                        });
                        break;
                    case "two":
                        phase.ifPresent(value -> {
                            filter.add(" current_date BETWEEN phaseTwoStart and phaseTwoEnd");
                        });
                        break;
                    case "finished":
                        phase.ifPresent(value -> {
                            filter.add(" phaseTwoEnd < current_date");
                        });
                        break;
                }
            }
            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }


            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<Contest> queryList = session.createQuery(queryString.toString(), Contest.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }
        switch (params[0]) {
            case "title":
                queryString.append(" title ");
                break;

        }
        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }

        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }
        return queryString.toString();
    }
}