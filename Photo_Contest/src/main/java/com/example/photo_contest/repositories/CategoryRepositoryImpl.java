package com.example.photo_contest.repositories;

import com.example.photo_contest.models.Category;
import com.example.photo_contest.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepository<Category> implements CategoryRepository {

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
    }

    @Override
    public Category getByName(String name) {
        return getByField("name", name);
    }
}
