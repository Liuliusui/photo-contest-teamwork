package com.example.photo_contest.repositories.contracts;

import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.User;
import com.example.photo_contest.models.enums.UserSortOption;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> getAll(Optional<String> search);

    User getByUsername(String name);

    User getByEmail(String email);

    List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> username, Optional<String> email, Optional<String> sort);

    List<User> filter(Optional<String> search, Optional<UserSortOption> sort);

    List<User> getOrganizers();


}
