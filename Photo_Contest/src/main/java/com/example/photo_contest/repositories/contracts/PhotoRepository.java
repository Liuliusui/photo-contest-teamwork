package com.example.photo_contest.repositories.contracts;

import com.example.photo_contest.models.Photo;

import java.util.List;
import java.util.Optional;

public interface PhotoRepository extends BaseCRUDRepository<Photo> {

    List<Photo> filter(Optional<String> search, Optional<String> sort);

    List<Photo> geAllFromContest(int id);

}
