package com.example.photo_contest.services;

import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.models.Category;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.example.photo_contest.Helpers.createMockPhoto;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceImplTests {

    @Mock
    PhotoRepository repository;

    @InjectMocks
    PhotoServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getAllFromContest_should_callRepository() {
        // Arrange
        Mockito.when(repository.geAllFromContest(1))
                .thenReturn(new ArrayList<>());

        // Act
        service.geAllFromContest(1);

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .geAllFromContest(1);
    }

    @Test
    public void create_should_callRepository() {
        // Arrange
        Photo mockPhoto = createMockPhoto();

        // Act
        service.create(mockPhoto);

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .create(mockPhoto);
    }


    @Test
    public void getById_should_returnPhoto_when_matchExist() {
        // Arrange
        Photo mockPhoto = createMockPhoto();
        Mockito.when(repository.getById(mockPhoto.getId()))
                .thenReturn(mockPhoto);
        // Act
        Photo result = service.getById(mockPhoto.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPhoto.getId(), result.getId()),
                () -> Assertions.assertEquals(mockPhoto.getTitle(), result.getTitle())
        );
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        Mockito.when(repository.filter(Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty());
    }



}
